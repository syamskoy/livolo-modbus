#include <xc.h>
#include <stdint.h>
#include "switch.h"
#include "config.h"
#include "util.h"
#include "modbus.h"

// For modbus
#define PIN_EN 2
#define ADDRESS 1
#define LED 13
uint16_t au16data[11];
//Modbus * slave;
unsigned long tempus;
int8_t state = 0;
Modbus slave(ADDRESS, PIN_EN);



/*
 * Public vars
 */

bit switch_status;

/*
 * Public functions
 */

/**
 * HFE60 datasheet says to not energize both set and reset coils at the
 * same time. Since PORTx output status is unknown on reset, let's pull
 * them down early
 * 
 * If R and S are pulled high at the same time the pic hangs and the whole
 * thing starts drawing a lot of current.
 */
void
switch_preinit()
{
    LED = LED_RED;
    switch_status = SWITCH_OFF;
    
    slave.begin(57600); 
}

void
switch_init()
{
    // Delay until the cap is charged before we can switch for the first time
    uint16_t t = POWERUP_TIME * 1000UL / 65536;
    uint8_t pwmt = 0;   // time
    uint8_t pwmd = 0;   // duty
    TMR1ON = 0;
    TMR1 = 0;
    TMR1ON = 1;
    while (t) {
        if (pwmt == pwmd || (pwmt ^ pwmd) == 0xff) LED = ~LED;
        if (++pwmt == 0) pwmd += POLICE_LIGHTS_FREQ;
        if (TMR1IF) { 
            t--;
            TMR1IF = 0;
        }
    }
    TMR1ON = 0;
    switch_off();
}


void switch_toggle()
{
    if (switch_status) {
        switch_off();
    } else {
        switch_on();
    }
    au16data[8] = switch_status;
    // For modbus
    // обработка сообщений
    state = slave.poll( au16data, 11); 
}


void switch_on(void)
{
    switch_status = SWITCH_ON;
    LED = LED_RED;
}

void switch_off(void)
{
    switch_status = SWITCH_OFF;
    LED = LED_BLUE;
}

